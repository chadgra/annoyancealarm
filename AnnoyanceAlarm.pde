#include <Wire.h>
#include <Adafruit_MCP23017.h>
#include <stdio.h>
#include <WaveHC.h>
#include <WaveUtil.h>
#include <NesController.h>
#include "RTClib.h"

#define POWER_BUTTON    8
#define RESET_BUTTON    9

// These #defines are for the different screen modes
#define TIME            0
#define FILE_RUN        2
#define MODE_COUNT      3

#define PIR_MOTION      7

#define TITLE_MAX       (20)
#define LINE_MAX        (20)

// Define macro to put error messages in flash memory.
#define error(msg) Serial.println(PSTR(msg))

RTC_DS1307 RTC;

SdReader card;    // This object holds the information for the card
FatVolume vol;    // This holds the information for the partition on the card
FatReader root;   // This holds the information for the volumes root directory
WaveHC wave;      // This is the only wave (audio) object, since we will only play one at a time

unsigned int consecutive_presses = 0;
unsigned long next_valid_press_time = 0;
int mode = TIME;
char title[TITLE_MAX] = { 0 };
FatReader song;

void setup()
{
    pinMode(PIR_MOTION, INPUT);     // declare sensor as input
    pinMode(POWER_BUTTON, INPUT);
    pinMode(RESET_BUTTON, INPUT);
    setupNesController1();

    // Debugging output
    Serial.begin(9600);
    Wire.begin();
    RTC.begin();

    // Set the display options.
    Wire.beginTransmission(0x38);
    Wire.write((uint8_t)0);
    Wire.write(B00010111);
    Wire.endTransmission();

    // Setup the file system:
    FreeRam();

    if (!card.init())
    {
        //error("Card init. failed!");
    }

    // enable optimize read - some cards may timeout. Disable if you're having problems
    card.partialBlockRead(true);

    // Now we will look for a FAT partition!
    uint8_t part;
    for (part = 0; part < 5; part++)
    {
        if (vol.init(card, part))
        {
            break;
        }
    }
    if (part == 5)
    {
        //error("No valid FAT partition!");
    }

    // Try to open the root directory
    if (!root.openRoot(vol))
    {
        //error("Can't open root dir!");
    }
}

void loop()
{
    DateTime now = RTC.now();
    uint8_t buttons = ValidatedButtonPress(readNesController1());
    static bool previous_motion = 0;
    bool motion = 0;
    bool end_early = false;
    long adjustment = 0;
    static FatReader file;
    int power = digitalRead(POWER_BUTTON);

    if (power)
    {
        motion = (bool)digitalRead(PIR_MOTION);
        //Serial.println(motion);
        if (motion && !previous_motion)
        {
            //Serial.println(mode);
            switch (mode)
            {
                case TIME:
                    mode = FILE_RUN;
                    if (file.isOpen())
                    {
                        //Serial.println("file was open");
                        file.close();
                    }

                    //Serial.println("about to open file");
                    file.open(root, (char *)"THEME.TXT");
                    file.seekSet(0);
                    //Serial.println("opened file");
                    break;
                case FILE_RUN:
                    end_early = true;
                    break;
                default:
                    break;
            }
        }

        previous_motion = motion;
    }
    else
    {
        wave.stop();
        mode = TIME;
        // This makes it so that it doesn't start playing right away.
        end_early = true;
    }

    if (buttons)
    {
        //Serial.println("buttons");
        switch (mode)
        {
            case TIME:
                adjustment = 50 + (consecutive_presses * 10);
                if (buttons & NES_UP)
                {
                    now = DateTime(now.unixtime() + adjustment);
                    RTC.adjust(now);
                }

                if (buttons & NES_DOWN)
                {
                    now = DateTime(now.unixtime() - adjustment);
                    RTC.adjust(now);
                }
                break;
            case FILE_RUN:
                // End the file if a button is pressed.
                mode = TIME;
                break;
            default:
                break;
        }
    }

    switch (mode)
    {
        case TIME:
            digitalClockDisplay(now);
            break;
        case FILE_RUN:
            //Serial.println("run");
            if (0 == runFile(&file, end_early))
            {
                //Serial.println("done");
                end_early = false;
                mode = TIME;
            }
            break;
        default:
            break;
    }
    for (int k = 0; k < 10000; k++);
}

// Time how often a button repeats so we don't get too many presses at once.
uint8_t ValidatedButtonPress(uint8_t buttons)
{
    unsigned long current_time = millis();

    if (buttons)
    {
        if (current_time >= next_valid_press_time)
        {
            consecutive_presses++;
            if (consecutive_presses == 1)
            {
                next_valid_press_time = current_time + 500;
            }
            else
            {
                next_valid_press_time += 100;
            }

            return buttons;
        }
    }
    else
    {
        consecutive_presses = 0;
        next_valid_press_time = 0;
    }

    return 0;
}

void digitalClockDisplay(DateTime now)
{
    // digital clock display of the time
    int hour = now.hour();
    bool isAM = (hour < 12) || (hour == 24);

    hour = hour > 12 ? hour - 12 : hour;

    hour *= 100;
    hour += now.minute();
    displayDigits(hour, true);
}

int runFile(FatReader *file, bool early_abort)
{
    static unsigned long end_time;
    static char buffer[LINE_MAX] = { 0 };
    static char end_song_title[TITLE_MAX] = { 0 };
    static char *song_title = NULL;
    static long change_ticks = 0;
    static long period = 1000;
    uint32_t position = (*file).readPosition();
    unsigned long current_time = millis();
    int ticks = 0;

    //Serial.print("early: ");
    //Serial.println(early_abort);
    if (early_abort)
    {
        if (0 != end_song_title[0])
        {
            playSong(end_song_title);
        }
        return 0;
    }

    if (current_time <= end_time)
    {
        ticks = (end_time - current_time) / period;
    }

    //Serial.print("position: ");
    //Serial.println(position);
    if (position == 0)
    {
        // Starting this file.
        if ((*file).isOpen())
        {
            wave.stop();
            end_song_title[0] = 0;
            do
            {
                // Get past any comments & special instructions.
                readLine(file, buffer, LINE_MAX);
                //Serial.println(buffer[0]);
                if (buffer[0] == '$')
                {
                    strncpy(end_song_title, &buffer[2], TITLE_MAX);
                }
            }
            while ((buffer[0] == ';') || (buffer[0] == '$'));

            // Initialize the time.
            song_title = parseLine(buffer, &change_ticks, &period);
            Serial.println(change_ticks);
            Serial.println(period);

            end_time = (change_ticks * period + period - 1) + current_time;
            Serial.println(current_time);
            Serial.println(end_time);
            ticks = (end_time - current_time) / period;
            Serial.println(ticks);

            // Start playing first song.
            playSong(song_title);

            // Read and parse the next line
            readLine(file, buffer, LINE_MAX);
            song_title = parseLine(buffer, &change_ticks, &period);
            //Serial.println(song_title);
        }
    }
    else if (ticks <= change_ticks)
    {
        playSong(song_title);
        // In the middle of this file.
        // Play the last loaded song
        if (position < (*file).fileSize())
        {
            readLine(file, buffer, LINE_MAX);
            song_title = parseLine(buffer, &change_ticks, &period);
        }
        else
        {
            change_ticks = -1;
        }
    }
    else if (0 >= ticks)
    {
        // It's over
        end_time = current_time - 1;
    }

    displayDigits(ticks, false);

    return ticks;
}

void playSong(char *song_title)
{
    wave.stop();
    if (song.open(root, song_title))
    {
        if (wave.create(song))
        {
            wave.play();
            Serial.println(song_title);
        }
    }
}

char *readLine(FatReader *file, char *buffer, int size)
{
    uint32_t position = (*file).readPosition();
    int16_t count = (*file).read(buffer, size);
    int i = 0;

    if (count < size)
    {
        buffer[count] = 0;
    }

    for (i = 0; i < count; i++)
    {
        if ((buffer[i] == '\r') || (buffer[i] == '\n'))
        {
            buffer[i] = 0;
            if (buffer[i + 1] >= 32)
            {
                i++;
                break;
            }
        }
    }

    (*file).seekSet(position + i);
    return buffer;
}

char *parseLine(char *buffer, long *time, long *period)
{
    int i = 0;
    long time_temp = 0;
    long period_temp = 0;
    bool new_period = false;

    while (((buffer[i] >= '0') && (buffer[i] <= '9')) || (buffer[i] == '#'))
    {
        if (buffer[i] == '#')
        {
            new_period = true;
        }
        else
        {
            if (new_period)
            {
                period_temp = (period_temp * 10) + (buffer[i] - '0');
            }
            else
            {
                time_temp = (time_temp * 10) + (buffer[i] - '0');
            }
        }

        i++;
    }

    while ((buffer[i] == ' ') || (buffer[i] == '\t'))
    {
        i++;
    }

    if (new_period)
    {
        *period = period_temp;
    }

    *time = time_temp;
    return &buffer[i];
}

void displayDigits(int value, bool decimal)
{
    const int lookup[10] = {0x3F,0x06,0x5B,0x4F,0x66, 0x6D,0x7D,0x07,0x7F,0x6F};
    int thousands, hundreds, tens, base;

    Wire.beginTransmission(0x38);
    Wire.write(1);

    thousands = value / 1000;
    hundreds = (value % 1000) / 100;
    tens = (value % 100) / 10;
    base = value % 10;

    Wire.write(lookup[base]);
    Wire.write((thousands || hundreds || tens) ? lookup[tens] : 0x00);
    Wire.write(((thousands || hundreds) ? lookup[hundreds] : 0x00) | (decimal ? 0x80 : 0x00));
    Wire.write(thousands ? lookup[thousands] : 0x00);
    Wire.endTransmission();
}
